#!/usr/bin/env python

####################################
#
# Creation date: 15.10.2018
#
# Author: Daniel Hofer <daniel.hofer@rwu.de>
#
####################################

from goal_publisher.msg import GoalPoint
from goal_publisher.msg import PointArray

import rospy

from std_msgs.msg import Header

import yaml


class GoalPublisher(object):
    def __init__(self):
        # Initiate a Node named 'goal_pub'
        rospy.init_node("goal_pub")

        # Getting launchfile parameter, with 'config.yaml' as default value
        config_file_name = rospy.get_param("~config_file", "config.yaml")
        self._publish_reward = rospy.get_param("~publish_reward", True)

        rospy.loginfo("Configuration file name: " + str(config_file_name))

        if self._publish_reward:
            rospy.loginfo("Goal rewards will be published.")
        else:
            rospy.loginfo("Goal rewards will not be published.")
        # Get base path of ros package
        path_to_open = config_file_name

        try:
            ymlfile = open(path_to_open, "r")
            self.configFile = yaml.load(ymlfile)
        except Exception as _:
            rospy.logerr("Could not open file " + str(path_to_open) + ".")
            rospy.logerr("Exiting")

        try:
            # Read topic name from config file
            topicname = self.configFile["topic_name"]
        except Exception as _:
            # Default name is /goals
            topicname = "/goals"

        # Create a publisher object, which will publish
        self.pub = rospy.Publisher(topicname, PointArray, queue_size=5)

    def start_publishing(self):
        """
        Publishes goals tot eh topic '/goals'. Will do so until the roscore
        is shut down. Reads the points from the config file and turns all
        well formed points (consisting of x, y and z value) into ros message points.
        Those are published in a custom message as an array.
        """
        # Publish rate in hz
        rate = rospy.Rate(2)

        # Create and init message object
        message = PointArray()
        message.header = Header()
        goals = []

        try:
            goals_yaml = self.configFile["goals"]
        except Exception as _:
            goals_yaml = None
            raise Exception("No tag 'goals' in yaml file found!")

        # Check if goals could be loaded
        if goals_yaml is not None:
            # Iterate through goals
            for name, goal in sorted(goals_yaml.items()):
                try:
                    if self._publish_reward:
                        reward = goal["reward"]
                    else:
                        reward = -1
                    # Add goal to list
                    goals.append(GoalPoint(goal["x"], goal["y"], goal["z"], reward))
                except Exception as _:
                    rospy.logerr(
                        """Could not add this point. Skipping it and continuing
                             with the next point"""
                    )
        else:
            raise Exception("Could not open yaml file with goals listed!")

        # Set goals to message
        message.goals = goals

        # Publish until roscore is shut down
        while not rospy.is_shutdown():
            # Check if someone is subscribing to the topic
            # If someone is subscribing, publish data, otherwise not
            if self.pub.get_num_connections() > 0:
                self.pub.publish(message)

            rate.sleep()


if __name__ == "__main__":
    pub = GoalPublisher()
    pub.start_publishing()
