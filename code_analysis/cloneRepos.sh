#!/bin/bash
rm -rf *_tier4

ws20="jm-191091 sj-182376 io-171737 ar-183031 jb-191343 jw-183109
fr-183033 sa-171186 ln-191048 lb-191042 je-171731 md-191038
jm-191051 mk-183098 sf-182406 dr-182378 jw-171604 gk-162242
vp-192150 sd-191300 sr-191320 sf-182503 eb-192129 ag-171751
st-182840 ha-191291 ms-171029 lj-191027 tn-202578 ra-171137
fb-161965 ja-171732 kn-192148 ld-183106
"

ss21="ag-171751 bk-191307 bm-161031 dj-212964 ea-201361
ec-192058 em-192077 ff-191493 gj-192061 gk-162242
hw-201370 ha-191291 io-171737 ja-182678 ja-201702
js-182393 mg-183073 mm-192026 ns-191045 mt-161658
ns-191189 ol-191310 pg-201084 ra-171137 sb-161023
sd-191300 se-191188 sr-191320 su-191330 th-192023
tm-183062 tn-192075 uk-171000"

for i in $ss21; do
	if [ $CI_JOB_TOKEN ]; then
	  repo=https://gitlab-ci-token:${CI_JOB_TOKEN}@fbe-gitlab.hs-weingarten.de/stud-amr/2021-ss-bachelor/$i\_tier4.git
	else
		repo=https://fbe-gitlab.hs-weingarten.de/stud-amr/2021-ss-bachelor/$i\_tier4.git
	fi
	git clone $repo
done
mkdir ss21 && mv *_tier4 ss21

for i in $ws20; do
	if [ $CI_JOB_TOKEN ]; then
	  repo=https://gitlab-ci-token:${CI_JOB_TOKEN}@fbe-gitlab.hs-weingarten.de/stud-amr/2020-ws-bachelor/$i\_tier4.git
	else
		repo=https://fbe-gitlab.hs-weingarten.de/stud-amr/2020-ws-bachelor/$i\_tier4.git
	fi
	git clone $repo
done
mkdir ws20 && mv *_tier4 ws20
