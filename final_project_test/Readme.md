# Start The Final Project Test
## Usage of start script
```
./start.sh <student_project> <gui>
```
#
`<student_project>` e.g. xx_123456_prj

`<gui>` default: true, disable: false or 0
### Headless example
```
./start.sh jm_191091_prj false
```
## Usage of rostest final_project_test
```
rostest final_project_test final.test PACKAGE:=<full_path_to_xx_123456_prj> ARENA:=<path_to_model.sdf> GOALS:=<path_to_goal_config.yaml> GUI:=<true_or_false>
```
### Headless example
```
rostest final_project_test final.test PACKAGE:=$(rospack find jm_191091_prj) ARENA:=$(rospack find final_project_test)/config/model.sdf GOALS:=$(rospack find final_project_test)/config/goals.yaml GUI:=false
```
