#!/usr/bin/env python3

from tf.transformations import euler_from_quaternion
from final_project_test.msg import StartActionGoal
from final_project_test.srv import RotateMsg
from goal_publisher.msg import PointArray
from gazebo_msgs.msg import ModelStates
from math import sqrt, pi
import re
import unittest
import rostest
import rospy
import time
import sys

PKG = 'final_project_test'


class ProjectTest(unittest.TestCase):
    final_action_server_exists = False

    def setUp(self):
        global rotate_service, sub_goals, sub_modelstates
        rotate_service = rospy.ServiceProxy('/rotate_now', RotateMsg, persistent=False, headers=None)
        sub_goals = rospy.Subscriber('/goals', PointArray, self.goal_callback)
        rospy.wait_for_message('/goals', PointArray)
        sub_modelstates = rospy.Subscriber('/gazebo/model_states', ModelStates, self.state_callback)
        rospy.wait_for_message('/gazebo/model_states', ModelStates)

        topic = StartActionGoal()
        topic.goal.start_driving = True
        pub_start = rospy.Publisher('/final_action_server/goal', StartActionGoal, queue_size=3)
        pub_start.publish(topic)

    def goal_callback(self, data):
        global goals
        goals = data.goals
        sub_goals.unregister()

    def state_callback(self, state):
        global x, y, yaw
        x = state.pose[state.name.index('turtlebot3_burger')].position.x
        y = state.pose[state.name.index('turtlebot3_burger')].position.y
        ori = state.pose[state.name.index('turtlebot3_burger')].orientation
        (wayne1, wayne2, yaw) = euler_from_quaternion([ori.x, ori.y, ori.z, ori.w])

    def test_a_action_server_available(self):
        global final_action_server_exists
        rospy.logwarn("Checking Topics ...")
        rospy.logwarn("...")
        topics = rospy.get_published_topics()

        expected_topic = r'final_action_server/goal'
        regexp = re.compile(expected_topic, re.DOTALL)
        final_action_server_exists = False

        for i in range(len(topics)):
            if regexp.search(str(topics[i])):
                final_action_server_exists = True
                rospy.logwarn(topics[i])
                rospy.logwarn("...")
                rospy.logwarn("-" * 50)
                rospy.logwarn("Action Server Up and Running!")
                rospy.logwarn("-" * 50)
                break
        if not final_action_server_exists:
            rospy.logwarn("-" * 50)
            rospy.logwarn("No Action Server named final_action_server exists!")
            rospy.logwarn("-" * 50)

        self.assertTrue(final_action_server_exists)

    def test_b_executing_final_project(self):
        if not final_action_server_exists:
            self.fail()
            return
        global reachedGoals, goals
        reachedGoals = []

        rospy.logwarn("Robot starting, 5 minutes runtime ...")
        rospy.logwarn("-" * 50)
        rospy.logwarn('{:<18s} {:<12s} {:<10s} {:<5s}'.format(
            "GOALPOINTS", "REWARDS", "SUM", "TIME(s)"))
        rospy.logwarn("".rjust(50, '-'))
        start_time = time.time()
        goal_cnt = 0
        sum = 0

        while True:
            for goal in goals:
                distance_to_turtlebot = self.distance(goal)
                if distance_to_turtlebot < 0.5:
                    goal_cnt += 1
                    sum += goal.reward
                    pt = str(goal_cnt) + ". (" + str(round(goal.x, 2)) + \
                        ", " + str(round(goal.y, 2)) + ")"
                    timeStr = str(round(time.time() - start_time, 2))
                    rospy.logwarn("{:<20s} {:<10s} {:<10s} {:<5s}".format(
                        pt, str(goal.reward),  str(sum), timeStr))
                    reachedGoals.append(goal)
                    goals.remove(goal)

                    # DEBUG
                    # if self.DEBUG(0):
                    #    return
                    #    DEBUG

                    if(len(goals) < 1):
                        rospy.logwarn("-" * 50)
                        rospy.logwarn("   All goals achieved   ".center(50, '+'))
                        rospy.logwarn("-" * 50)
                        return

            if time.time() - start_time > 300:
                rospy.logwarn("-" * 50)
                rospy.logwarn("  TIME IS UP  ".center(50, '-'))
                rospy.logwarn("-" * 50)
                rospy.logwarn("   {:<d} Goals achieved   " .center(50, '-').format(len(reachedGoals)))
                rospy.logwarn("-" * 50)
                break
            rospy.Rate(10).sleep()

    def DEBUG(self, max):
        if len(reachedGoals) > max:
            rospy.logwarn("-" * 50)
            rospy.logwarn("  TIME IS UP  ".center(50, '-'))
            rospy.logwarn("-" * 50)
            rospy.logwarn("   {:<d} goals achieved   ".center(
                54, '-').format(len(reachedGoals)))
            rospy.logwarn("-" * 50)
            return True
        return False
        # END DEBUG

    def test_c_reach_one_goal(self):
        self.assertTrue(len(reachedGoals) > 0)

    def test_d_reach_more_than_one_goal(self):
        self.assertTrue(len(reachedGoals) > 1)

    def test_e_react_on_service_call(self):
        if not final_action_server_exists:
            self.fail()
            return
        degrees = 180
        clockwise = True
        direction = "clockwise"
        rospy.logwarn("Checking Rotation Service ...")
        rospy.logwarn("-" * 50)
        # rospy.wait_for_service('rotate_now',timeout=5

        self.assertTrue(self.get_rotation_service(degrees, clockwise))
        rospy.logwarn("Rotation service is available")
        rospy.logwarn("-" * 50)
        rospy.logwarn("Calling /rotate_now %d° %s" %
                      (degrees, direction))
        rospy.logwarn("-" * 50)

        rospy.sleep(10)
        #kill = Popen(["pkill", "-f", "-9", "ros"], stdout=PIPE)

        #self.assertTrue(self.check_rotation_service(degrees, direction, clockwise))
        #rospy.logwarn("-" * 50)

    def get_rotation_service(self, degrees, clockwise):
        try:
            rotate_service(degrees, clockwise)
            return True
        except rospy.ROSException as exc:
            rospy.logwarn("Rotation service not available\n" + str(exc))
        except rospy.ServiceException as sexc:
            rospy.logwarn("Rotation service not available\n" + str(sexc))
        rospy.logwarn("-" * 50)
        return False

    def check_rotation_service(self, degrees, direction, clockwise):
        covered_angular_degrees = timeout = 0
        start_point = previous = self.get_radian()
        start_time = time.time()
        rospy.logwarn('{:>1s}'.format(
            "Turtlebots angular motion: "))
        rospy.logwarn("-" * 50)
        rospy.Rate(3).sleep()
        while abs(degrees - covered_angular_degrees) > 5:
            if time.time() - start_time > 30:
                rospy.logwarn("-" * 50)
                rospy.logwarn("Rotation Service Timeout ...")
                rospy.logwarn("-" * 50)
                return False
                return
            current_radian = self.get_radian()
            current_degree = int(current_radian * 180 / pi)

            if not clockwise and previous > current_degree or clockwise and previous < current_degree:
                previous = current_degree
                timeout += 1
                if timeout > 20:
                    rospy.logwarn("you should be turning %s! previous: %d° current: %d°" %
                                  (direction, previous, current_degree))
                    return False
                    return
                rospy.Rate(5).sleep()
                continue

            if clockwise:
                covered_angular_radian = 2 * pi - current_radian + start_point
            else:
                covered_angular_radian = 2 * pi + current_radian - start_point
            if covered_angular_radian >= 2 * pi:
                covered_angular_radian -= 2 * pi
            covered_angular_degrees = int(covered_angular_radian * 180 / pi)

            rospy.logwarn("{:>5s} {:>1s} {:>5s} {:>8s}".format(
                str(covered_angular_degrees) + "°", " | ",  str(round(covered_angular_radian, 2)), " radians"))
            previous = current_degree
            rospy.Rate(5).sleep()

        rospy.logwarn("-" * 50)
        rospy.logwarn(" Rotation service successfully performed ".center(50, "-"))
        return True

    def get_radian(self):
        radians = yaw
        if yaw < 0:
            radians = 2 * pi - abs(yaw)
        return radians

    def distance(self, goal):
        return sqrt((x - goal.x)**2 + (y - goal.y)**2)


if __name__ == '__main__':
    rospy.init_node('final_project_test', anonymous=True)
    # if gui is enabled, wait for nodes to launch
    rospy.sleep(int(sys.argv[1]))
    rostest.rosrun(PKG, 'test_fp', ProjectTest, sys.argv)
