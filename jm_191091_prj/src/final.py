#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import rospy
import time
import random
import rotate
from jm_191091_prj.srv import RotateMsg
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from gazebo_msgs.msg import ModelStates
from math import sin, cos, atan2, pi, sqrt, pow
from tf.transformations import euler_from_quaternion
from goal_publisher.msg import PointArray, GoalPoint


class Turtlebot:
    hz = 20  # refresh rate
    laser = None  # LaserScan()
    goals = None  # PointArray()
    move = Twist()  # publisher msg

    cnt = sum = start = elapsed = total = 0
    noTarget = True
    loop = back = False
    goal = tempGoal = GoalPoint()

    rotate = False
    rotationDegrees = 0

    def __init__(self):
        self.rotation_service = rospy.Service('/rotate_now', RotateMsg, self.rotationHandler)
        self.publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=3)
        rospy.Subscriber("/scan", LaserScan, self.laser_callback)
        rospy.Subscriber('/goals', PointArray, self.goal_callback)
        rospy.Subscriber('/gazebo/model_states', ModelStates, self.state_callback)

    def laser_callback(self, laser):
        self.laser = laser

    def goal_callback(self, pointarray):
        self.goals = pointarray.goals

    def state_callback(self, state):
        self.x = state.pose[state.name.index('turtlebot3_burger')].position.x
        self.y = state.pose[state.name.index('turtlebot3_burger')].position.y
        ori = state.pose[state.name.index('turtlebot3_burger')].orientation
        (wayne1, wayne2, self.yaw) = euler_from_quaternion([ori.x, ori.y, ori.z, ori.w])

    def rotationHandler(self, msg):
        self.rotate = True
        self.degrees = msg.degrees
        self.clockwise = msg.clockwise
        return []

    def get_min_laserdata(self):
        self.F = min(self.laser.ranges[0:10] + self.laser.ranges[350:361])
        self.L = min(self.laser.ranges[80:100])
        self.R = min(self.laser.ranges[260:280])
        self.closestPt = min(self.laser.ranges[0:100] + self.laser.ranges[260:361])

    # continuously update laserdata, current orientation and closest obstacle points
    # and calculate linear x and angular z velocity
    def start(self):
        while not self.laser or not self.goals:
            rospy.Rate(2).sleep()
        print("".rjust(50, '-'))
        print('{:<18s} {:<12s} {:<10s} {:<5s}'.format(
            "GOALPOINTS", "REWARDS", "SUM", "TIME"))
        print("".rjust(50, '-'))

        self.start = self.total = time.time()
        self.unvisited = self.goals
        while not rospy.is_shutdown():

            if self.rotate:
                self.rotate = False
                rotate.RotationService().start_rotate(self.degrees, self.clockwise)
                self.rotation_service.shutdown()
            else:
                self.get_min_laserdata()
                z = self.angular_speed()
                if(z is None):  # all goals reached, shutdown
                    break
                x = self.linear_speed()
                self.publish_move(x, z)
                rospy.Rate(20).sleep()
        self.publish_move(0, 0)
        print("".center(50, '-'))
        print(" DONE ".center(50, '-'))
        print("".center(50, '-'))

    def publish_move(self, x, z):
        self.move.linear.x = x
        self.move.angular.z = z
        self.publisher.publish(self.move)

    # Returns appropriate linear x velocity
    def linear_speed(self):
        distance = self.closestPt
        if distance > 1:
            distance = 1
        goaldistance = self.get_distance(self.goal)
        x = self.move.linear.x
        if self.back:  # reverse gear for 2 secs
            if time.time() - self.backtime > 2:
                self.back = False
                x = 0.5 * distance
            x = -distance
        elif self.F <= 0.2:  # switch into reverse gear
            self.back = True
            self.backtime = time.time()
        elif self.F <= 0.3:
            x = 0.2 * self.F  # obstacle too close infront
        elif self.F <= 0.5 and x >= 0.15:
            x = 0.3 * self.F  # obstacle too close infront
        elif self.F <= 0.75 and x >= 0.2:
            x = 0.35 * self.F  # obstacle infront
        elif self.F <= 1 and x >= 0.4:
            x = 0.4 * self.F  # obstacle infront
        elif self.F <= 1.3 and x >= 0.6:
            x = 0.45 * self.F  # obstacle infront
        elif self.F <= 2 and x >= 0.8:
            x = 0.5 * self.F  # obstacle infront
        elif abs(self.error) > 0.5 and x > 0.4:
            x *= 0.8  # antidrift
        elif goaldistance <= 0.8 and x > 0.6:
            x -= 0.75 / self.hz
        elif abs(self.error) < 0.1 and self.L <= 0.5 or self.R <= 0.5:
            if x < 3 * distance:  # obstacle on either side, but not infrnt -> speed up
                x += 0.25 / self.hz
            elif x > 3 * distance:
                x -= 0.25 / self.hz
        elif abs(self.error) < 0.1 and self.L <= 1 or self.R <= 1:
            if x < 2 * distance:  # obstacle on either side, but not infrnt -> speed up
                x += 0.25 / self.hz
            elif x > 2 * distance:
                x -= 0.25 / self.hz
        elif abs(self.error) < 0.1 and self.F > 2 and self.closestPt > 1:
            if x < distance:  # full speed
                x += 0.25 / self.hz
        return x

    # Returns angular z velocity which is proportional to the error between goalangle and yaw,
    # rotating fast when the error is big, and rotating slower the smaller the error gets
    # inspired by https://www.theconstructsim.com/ros-qa-053-how-to-move-a-robot-to-a-certain-point-using-twist/
    def angular_speed(self):
        goal = self.get_goalpoint()
        if(goal is None):
            return None

        # 1. Determine goalangle
        goalAngle = atan2(goal.y - self.y, goal.x - self.x)
        if goalAngle < 0:  # adjust goalAngle if it overshoots [0,2PI]
            goalAngle += 2 * pi
        elif goalAngle > 2 * pi:
            goalAngle -= 2 * pi

        # 2. determine the error between goalAngle and yaw
        error = goalAngle - self.yaw
        if error > pi:  # adjust error if it overshoots [-pi,pi]
            error -= 2 * pi
        elif error < -pi:
            error += 2 * pi
        self.error = error

        # 3. Determine whether the error is small enough, too small or too big
        if error > 0.01:  # error too big, turn left
            return 1.5 * abs(error)
        elif error < 0.01:  # error too small, turn right
            return -1.5 * abs(error)
        return 0  # error small enough, dont rotate

    # Determine goalpoint
    def get_goalpoint(self):
        goal = None
        delta = time.time() - self.start

        if (self.closestPt < 0.5):  # either obstacle too close -> drive around obstacle
            self.noTarget = True
            goal = self.avoid_obstacle_crash()
        elif delta > 20 and self.loop:
            self.reset(False)
            self.start = time.time()
        elif delta > 60:
            self.reset(True)
            self.start = time.time()
        elif self.noTarget and not self.loop:  # or pick closest goal
            self.reset(False)

        if self.goal_reached():
            if(self.cnt >= len(self.goals)):
                return None
            self.start = time.time()
            self.reset(False)

        if(goal is None):
            return self.goal
        return goal

    def reset(self, loop):
        self.loop = loop
        self.noTarget = False
        self.goal = self.pick_a_goal()

    def pick_a_goal(self):
        min = float('inf')
        for i in range(len(self.unvisited)):  # pick closest
            distance = self.get_distance(self.unvisited[i])
            if distance < min:
                min = distance
                index = i
        if self.loop and len(self.unvisited) > 2:  # hunting too long, pick random
            notwanted = index  # we dont want to pick the same goal that leads to cycles
            while True:  # sophisticated loop prevention xD
                index = int((random.random() * 100) % (len(self.unvisited)))
                if index != notwanted and self.unvisited[index].reward > 1:
                    break  # bingo
        return self.unvisited[index]

    def goal_reached(self):
        for i in range(len(self.unvisited)):
            if self.get_distance(self.unvisited[i]) < 0.5:
                self.cnt += 1
                self.sum += self.unvisited[i].reward
                pt = self.unvisited[i]
                str1 = str(self.cnt) + ". (" + str(round(pt.x, 2)) + ", " + str(round(pt.y, 2)) + ")"
                str4 = str(round(time.time() - self.total, 2))
                print('{:<20s} {:<10s} {:<10s} {:<5s} '.format(
                    str1, str(pt.reward),  str(self.sum), str4))
                self.unvisited.remove(self.unvisited[i])
                return True
        return False

    # Returns a goalpoint which the robot uses to drive parallel along the obstacle
    def avoid_obstacle_crash(self):
        if self.F < 0.5:
            radIndex = self.laser.ranges.index(self.F) * pi / 180  # convert to radians
        else:
            radIndex = self.laser.ranges.index(self.closestPt) * pi / 180  # convert to radians
        if self.F <= 0.2:
            heuristic = pi / 2 + pi / 6
        elif self.F <= 0.3:
            heuristic = pi / 2 + pi / 12
        else:
            heuristic = pi / 2
        if radIndex <= pi:  # obstacle on the left side
            goalAngle = self.yaw + radIndex - heuristic

        elif radIndex > pi:  # obstacle on the right side
            goalAngle = self.yaw - (2 * pi - radIndex) + heuristic

        if goalAngle > 2 * pi:  # keep angle within 2*pi
            goalAngle -= 2 * pi

        self.tempGoal.x = 3.5 * cos(goalAngle) + self.x
        self.tempGoal.y = 3.5 * sin(goalAngle) + self.y
        return self.tempGoal

    def get_distance(self, goal):
        return sqrt(pow(goal.x - self.x, 2) + pow(goal.y - self.y, 2))
