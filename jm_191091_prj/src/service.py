#! /usr/bin/env python3
import final
import rospy
import actionlib
from jm_191091_prj.msg import StartAction, StartFeedback, StartResult


class Service:
    actionserver_feedback = StartFeedback()
    actionserver_result = StartResult()

    def __init__(self):
        self.action_server = actionlib.SimpleActionServer(
            "final_action_server", StartAction, self.messageHandler, auto_start=False)
        self.action_server.start()

    def messageHandler(self, msg):
        if msg.start_driving is True:
            self.actionserver_feedback.current_state = "starting Robot"
            self.action_server.publish_feedback(self.actionserver_feedback)
            final.Turtlebot().start()
        else:
            print('NOT STARTING')
            self.actionserver_result.final_state = "NOT STARTING"
            self.action_server.set_succeeded(self.actionserver_result)
            return self.actionserver_result

        self.actionserver_result.final_state = "Successfully started"
        self.action_server.set_succeeded(self.actionserver_result)
        return self.actionserver_result


if __name__ == '__main__':
    rospy.init_node('final_prj_service_node', anonymous=True)
    server = Service()
    rospy.spin()  # keep service open
