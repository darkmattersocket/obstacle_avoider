#! /usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from math import sin, cos, atan2, pi
from gazebo_msgs.msg import ModelStates
from geometry_msgs.msg import Twist
from tf.transformations import euler_from_quaternion
from goal_publisher.msg import GoalPoint


class RotationService:

    yaw = None

    def __init__(self):
        self.pub = rospy.Publisher('/cmd_vel', Twist, queue_size=3)
        rospy.Subscriber('/gazebo/model_states', ModelStates, self.state_callback)
        rospy.wait_for_message('/gazebo/model_states', ModelStates)
        self.move = Twist()

    def state_callback(self, state):
        self.x = state.pose[state.name.index('turtlebot3_burger')].position.x
        self.y = state.pose[state.name.index('turtlebot3_burger')].position.y
        ori = state.pose[state.name.index('turtlebot3_burger')].orientation
        (wayne1, wayne2, self.yaw) = euler_from_quaternion([ori.x, ori.y, ori.z, ori.w])

    def start_rotate(self, degrees, clockwise):
        print("MSG: degrees: %d, clockwise: %r" % (degrees, clockwise))
        goal = GoalPoint()

        while not self.yaw:
            rospy.Rate(5).sleep()
        if clockwise:
            direction = "right"
            vorzeichen = -1
            angle = self.yaw - degrees * pi / 180
        else:
            direction = "left"
            vorzeichen = 1
            angle = self.yaw + degrees * pi / 180

        goal.x = 3.5 * cos(angle) + self.x
        goal.y = 3.5 * sin(angle) + self.y

        goalAngle = atan2(goal.y - self.y, goal.x - self.x)
        if goalAngle < 0:  # adjust goalAngle if it overshoots [0,2PI]
            goalAngle += 2 * pi
        elif goalAngle > 2 * pi:
            goalAngle -= 2 * pi
        error = goalAngle - self.yaw

        start = self.get_radian()
        covered_angular_degrees = 0
        while abs(error) > 0.0003:
            current_radian = self.get_radian()

            error = goalAngle - self.yaw
            if error > pi:  # adjust error if it overshoots [-pi,pi]
                error -= 2 * pi
            elif error < -pi:
                error += 2 * pi

            # print("error: %.5f" % abs(error))

            if abs(error) < 0.0005:
                rotational_speed = 10 * abs(error)
            elif abs(error) < 0.001:
                rotational_speed = 7.5 * abs(error)
            elif abs(error) < 0.01:
                rotational_speed = 5 * abs(error)
            elif abs(error) < 0.1:
                rotational_speed = 2.5 * abs(error)
            elif abs(error) < 1:
                rotational_speed = 2 * abs(error)
            else:
                rotational_speed = 1.5 * abs(error)

            if clockwise:
                covered_angular_radian = 2 * pi - current_radian + start
            else:
                covered_angular_radian = 2 * pi + current_radian - start
            rotational_speed *= vorzeichen

            if covered_angular_radian >= 2 * pi:
                covered_angular_radian -= 2 * pi
            covered_angular_degrees = covered_angular_radian * 180 / pi

            self.publish_move(0, rotational_speed)
            rospy.Rate(20).sleep()

        self.publish_move(0, 0)
        print("-" * 50)
        print("Rotatet %.2f° to the %s" % (covered_angular_degrees, direction))
        print("-" * 50)

    def get_radian(self):
        radians = self.yaw
        if self.yaw < 0:
            radians = 2 * pi - abs(self.yaw)
        return radians

    def publish_move(self, x, z):
        self.move.linear.x = x
        self.move.angular.z = z
        self.pub.publish(self.move)
